# Gruvbox colorscheme for your LaTeX documents

Use your favorite colorscheme to theme your entire document with this package.
```tex
\usepackage[dark]{gruvbox-colorscheme}
```

It define the following colors and their names (light):

- background `#fbf1c7`
- font `#3c3836`
- red `#cc241d`
- green `#98971a`
- yellow `#d79921`
- blue `#458588`
- purple `#b16286`
- aqua `#689d6a`
- gray `#7c6f64`
- orange `#d65d0e`

And with the `dark` option enabled:

- background `#282828`
- font `#ebdbb2`
- gray `#a89984`
